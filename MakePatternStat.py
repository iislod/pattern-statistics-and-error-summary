#coding=utf-8
import sys, os
import csv, codecs

def Read(FileName):

    Obj = {}
    data = []
    with open(FileName) as f:
        dat = csv.reader(f, delimiter=',', quotechar='"')
        for i in dat:
            data.append(i)

    colnames = data[0]
    colnames[0] = colnames[0].decode("utf-8-sig")[1:-1]

    objid = 'ID'
    for linenum, line in enumerate(data[1:], 1):
        for e, x in enumerate(line):
            if x != '':
                x = x.replace(",", "，")
                if e == 0:
                    objid = x
                    if not Obj.get(objid): Obj[objid] = {}
                    Obj[objid][colnames[e]] = (str(linenum), x)
                else:
                    if colnames[e] not in Obj[objid]:
                        Obj[objid][colnames[e]] = []
                    Obj[objid][colnames[e]].append((str(linenum), x.decode('utf8')))

    return Obj, colnames, data

def CSVWRITETOFILE(X, filename):
    
    f = open(filename, "w")
    f.write(codecs.BOM_UTF8)
    writer = csv.writer(f)
    writer.writerows(X)
    f.close()

def PrintCOL(Object, colnames, n, t=1, s=1):
    
    X = {}
    missing = []
    for i in Object:
        if colnames[n-1] in Object[i]:
            for j in Object[i][colnames[n-1]]:
                if j[1] in X:
                    X[j[1]] += 1
                else:
                    X[j[1]] = 1
        else:
            missing.append(i)

    return X, missing

def TransHierar(Obj, colnames, itemnum, t=1, s=1): 
    
    HieObj = {}
    hiercol = []
    for i in itemnum:
        hiercol.append(colnames[i-1])
    
    for i in Obj:
        if hiercol[0] in Obj[i] and hiercol[1] in Obj[i]:
            col1 = Obj[i][hiercol[0]]
            col2 = Obj[i][hiercol[1]]
            for value in col2:
                find = 0
                for e, key in enumerate(col1):
                    if value[0] == key[0]:
                        if key[1] not in HieObj:
                            HieObj[key[1]] = {}
                        if value[1] not in HieObj[key[1]]:
                            HieObj[key[1]][value[1]] = []
                        HieObj[key[1]][value[1]].append(i)
                        samefield = 1
                        break
                    
                if find == 0 and hiercol[0] == 'Project' and 'Project' not in hiercol[1]:
                    key = col1[0]
                    if key[1] not in HieObj:
                        HieObj[key[1]] = {}
                    if value[1] not in HieObj[key[1]]:
                        HieObj[key[1]][value[1]] = []
                    HieObj[key[1]][value[1]].append(i)
                    
        elif hiercol[1] in Obj[i]:
            key = "No Field"
            col2 = Obj[i][hiercol[1]]
            for value in col2:
                if key not in HieObj:
                    HieObj[key] = {}
                if value[1] not in HieObj[key]:
                    HieObj[key][value[1]] = []
                HieObj[key][value[1]].append(i)

    store = []
    for i in sorted(HieObj):
        store.append(i)
        for j in sorted(HieObj[i]):
            store.append("%s%s%s%s%s" %('\t|---', j, '(', len(HieObj[i][j]), ')'))

    return HieObj, store

def ColumnPair(colnames):

    Pair = {}
    fieldid = {}
    for e, i in enumerate(colnames, 1):
        if "field" in i:
            fieldid[i.split("::")[0]] = e
        elif i == 'Project::GenDate':
            fieldid[i] = e
        elif i in fieldid:
            Pair[e] = fieldid[i]
        elif i == 'Project':
            Pair[fieldid['Project::GenDate']] = e

    return Pair

def TransToStat(X):
    
    item = []
    num = []
    
    for i in sorted(X):
        count = 0
        for j in sorted(X[i]):
            item.append(j)
            num.append(str(len(X[i][j])))
            count += len(X[i][j])
        item.append("total(%s)" %(i))
        num.append(str(count))

    return item, num

def TransColToStat(X):

    item = []
    num = []

    count = 0
    for i in sorted(X):
        item.append(i)
        num.append(str(X[i]))
        count += X[i]
    item.append("total")
    num.append(str(count))
    
    return item, num

def Draw_pattern_stat(Object, colnames, Filename):
    
    Pair = ColumnPair(colnames)
    StoreDat = []
    for e, cn in enumerate(colnames, 1):
        if cn in ['OID', 'DAURI', 'Hyperlink', 'ICON', 'DigiArchiveID', 'Identifier']:
            StoreDat.append([cn])
        elif e in Pair and cn != 'Project':
            if cn == 'Project::GenDate':
                X, store = TransHierar(Object, colnames, [Pair[e], e])
            else:
                X, store = TransHierar(Object, colnames, [Pair[e], e])
            item, num = TransToStat(X)
            StoreDat.append([cn] + item)
            StoreDat.append(["No"] + num)
        else:
            X, missing = PrintCOL(Object, colnames, e)
            item, num = TransColToStat(X)
            StoreDat.append([cn] + item)
            StoreDat.append(["No"] + num)

    StoreDatT = []
    collen = {e: len(i) for e, i in enumerate(StoreDat)}
    maxrow = max(collen.values())
    for row in range(maxrow):
        content = []
        for e, col in enumerate(StoreDat):
            if row < collen[e]:
                content.append(StoreDat[e][row].encode('utf-8'))
            else:
                content.append("")
        StoreDatT.append(content)

    CSVWRITETOFILE(StoreDatT, Filename)

    return StoreDat, StoreDatT

if __name__ == "__main__":
  
    csvname = sys.argv[1]

    input_dir = ""
    
    Object, colnames, data = Read(input_dir + csvname + ".csv")
    print "Row: %d" %(len(data))
    print "OID: %d" %(len(Object))
    print "Column: %d" %(len(colnames))

    Draw_pattern_stat(Object, colnames, "stat/pattern/PatternStatistics-" + csvname + ".csv")    
