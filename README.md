# Tool for Generating Pattern Statistics and Error Summary

author: Hsin-Ping Chen

# Introduction

This tool is designed to generate pattern statistics (for csv files) and error summary (for csv and xml files) for csv files converted from xml files of Union Catalog of Digital Archives Taiwan.

* Input: csv files converted from xml files of Union Catalog of Digital Archives Taiwan

* Output: pattern statistics and error summary of xml and csv files

# Dependencies

* Python 2.7

* Python dependencies

```
pip install -r requirements.txt
```

* Only tested on Ubuntu (>= 14.04)

```
sudo apt-get install build-essential python-dev
```

# Usage

1. Change `MakePatternStat.py` line 194 to the path of directory containing input csv files.

2. Change `errorcheck.py` line 499 to the path of directory containing input csv files.

3. Run `bash Run.sh datalist.txt`

4. Results can be found in `error/` and `stat/`.
