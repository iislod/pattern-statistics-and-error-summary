FILENAME=$1
cat $FILENAME | while read LINE
do
    echo $LINE
    
    echo "Step 1. Make the PatternStat"
    mkdir -p stat/pattern
    python MakePatternStat.py $LINE

    echo "Step 2. Check XML, CSV error (error code)"
    mkdir -p error/$LINE
    python errorcheck.py $LINE

done
