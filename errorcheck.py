#coding=utf-8
import sys, os
import csv, codecs
from dateutil import parser
import time
import datetime
import httplib
from urlparse import urlparse
import re

import unicodedata


def Read(FileName):

    Obj = {}
    data = []
    with open(FileName) as f:
        dat = csv.reader(f, delimiter=',', quotechar='"')
        for i in dat:
            data.append(i)

    colnames = data[0]
    colnames[0] = colnames[0].decode("utf-8-sig")[1:-1]

    objid = 'ID'
    for linenum, line in enumerate(data[1:], 1):
        for e, x in enumerate(line):
            if x != '':
                x = x.replace(",", "，")
                if e == 0:
                    objid = x
                    #Obj[objid] = {}
                    if not Obj.get(objid): Obj[objid] = {}
                    Obj[objid][colnames[e]] = (str(linenum), x)
                else:
                    if colnames[e] not in Obj[objid]:
                        Obj[objid][colnames[e]] = []
                    Obj[objid][colnames[e]].append((str(linenum), x.decode('utf8')))

    return Obj, colnames

def CSVWRITETOFILE(X, filename):
    
    #print filename
    f = open(filename, "w")
    f.write(codecs.BOM_UTF8)
    writer = csv.writer(f, delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL)
    writer.writerows(X)
    f.close()

def CharIsInCOL(Object, colnames, itemnum, char):

    OIDLIST = []
    for i in Object:
        if colnames[itemnum-1] in Object[i]:
            obj = Object[i][colnames[itemnum-1]]
            for j in obj:
                if char in j[1]:
                    OIDLIST.append(i)
    
    return list(set(OIDLIST))

def MatchFormat(string, formats):
    
    r = re.compile(formats)
    if r.match(string):
        return 1
    else:
        return 0

def TransToXML(url):
    ### URL : http://catalog.digitalarchives.tw/item/00/00/46/14.html ###
    ### TO: http://140.109.18.115/showxml/00/00/46/14.xml ###

    temp = url.split('/')
    item = ""
    get = 0
    for i in temp:
        if i == "item":
            get = 1
            continue
        if get == 1:
            item += i + '/'
    
    newurl = "http://140.109.18.115/showxml/" + item.split('.')[0] + '.xml'
    return newurl

def MakeFile(Object, colnames, OIDLIST, FileName):
    
    store = [[colnames[0], colnames[1], "XMLURL", "Title", "Record"]]
    for i in OIDLIST:
        record = ""
        if 'Record' in Object[i]:
            for j in Object[i]['Record']:
                if "典藏機構與計畫".decode('utf8') in j[1]:
                    record = j[1].encode('utf8')
        OID = Object[i]['OID'][1]

        if 'DAURI' not in Object[i]:
            DAURI = ''
            XML = ''
        else:
            DAURI = Object[i]['DAURI'][0][1]
            XML = TransToXML(Object[i]['DAURI'][0][1])

        if 'Title' not in Object[i]:
            Title = ''
        else:
            TITLE = Object[i]['Title'][0][1].encode('utf8')
        
        store.append([OID, DAURI, XML, TITLE, record])

    CSVWRITETOFILE(store, FileName)

def MakeFile_withCon(Object, colnames, ErrorList, FileName):
    
    store = [[colnames[0], colnames[1], "XMLURL", "Title", "Record", "Error Value"]]
    OIDLIST = [i[0] for i in ErrorList]

    for e, i in enumerate(OIDLIST):
        record = ""
        if 'Record' in Object[i]:
            for j in Object[i]['Record']:
                if "典藏機構與計畫".decode('utf8') in j[1]:
                    record = j[1].encode('utf8')
        OID = Object[i]['OID'][1]

        if 'DAURI' not in Object[i]:
            DAURI = ''
            XML = ''
        else:
            DAURI = Object[i]['DAURI'][0][1]
            XML = TransToXML(Object[i]['DAURI'][0][1])

        if 'Title' not in Object[i]:
            Title = ''
        else:
            TITLE = Object[i]['Title'][0][1].encode('utf8')

        con = ErrorList[e][1].encode('utf8')
        store.append([OID, DAURI, XML, TITLE, record, con])

    CSVWRITETOFILE(store, FileName)

def checkUrl(url):
    
    p = urlparse(url)
    conn = httplib.HTTPConnection(p.netloc)
    conn.request('HEAD', p.path)
    resp = conn.getresponse()
    
    return resp.status

def Segment(textstring):
    textstring = unicodedata.normalize('NFKC', textstring)
    textstring = textstring.replace('。'.decode('utf8'),'.')

    return re.split('\W', textstring, flags=re.UNICODE)

def guess_seq_len(seq):
    
    guess = 1
    stringtoken = Segment(seq)
    if len(stringtoken) < 2:
        return 1, stringtoken

    max_len = len(stringtoken) / 2
    for x in range(2, max_len):
        if stringtoken[0:x] == stringtoken[x:2*x] :
            return x, stringtoken

    return guess, stringtoken


def Check_error_4011(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []
   
    for i in Object:
        ot = Object[i]['Project::GenDate'][0][1]
        iscorrect = 0
        if MatchFormat(ot, '\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d.\d'):
            iscorrect = 1
        elif MatchFormat(ot, '\d\d\d\d/\d\d/\d\d \d\d:\d\d:\d\d.\d'):
            iscorrect = 1
        elif MatchFormat(ot, '\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d'):
            iscorrect = 1
        elif MatchFormat(ot, '\d\d\d\d/\d\d/\d\d \d\d:\d\d:\d\d'):
            iscorrect = 1
        elif MatchFormat(ot, '\d\d\d\d-\d\d-\d\d'):
            iscorrect = 1
        elif MatchFormat(ot, '\d\d\d\d/\d\d/\d\d'):
            iscorrect = 1
        
        if iscorrect == 0:
            OIDLIST.append(i)
            ErrorList.append((i, ot))

    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')

    return OIDLIST


def Check_error_4012(Object, colnames, FileName):
    
    ts = time.time()
    Now = datetime.datetime.fromtimestamp(ts)
    ##Now = parser.parse("2009-10-13 02:22:05")
    OIDLIST = []
    ErrorList = []

    #print "Now", Now
    for i in Object:
        ot = parser.parse(Object[i]['Project::GenDate'][0][1])
        if ot > Now:
            OIDLIST.append(i)
            ErrorList.append((i, ot))

    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')

    return OIDLIST

def Check_error_4021(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []

    for i in Object:
        content_exist = 0 #### 內容主題 ####
        project_exist = 0 #### 典藏機構與計畫 ####
        for j in Object[i]['Record']:
            if '典藏機構與計畫'.decode('utf8') in j[1]:
                project_exist = 1
            if '內容主題'.decode('utf8') in j[1]:
                content_exist = 1
        if content_exist == 0 or project_exist == 0:
            OIDLIST.append(i)
            ErrorList.append((i, "(內容主題: %d, 典藏機構與計畫: %d)" %(content_exist, project_exist)))
            
    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')
    return OIDLIST

def Check_error_4022(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []

    for i in Object:
        for j in Object[i]['Record']:
            if '：'.decode('utf8') in j[1]:
                OIDLIST.append(i)
                ErrorList.append((i, j[1]))

    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')
    return OIDLIST

def Check_error_4023(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []

    for i in Object:
        for j in Object[i]['Record']:
            if '::' in j[1]:
                OIDLIST.append(i)
                ErrorList.append((i, j[1]))
            elif j[1].endswith(':'):
                OIDLIST.append(i)
                ErrorList.append((i, j[1]))
            
            if '典藏機構與計畫'.decode('utf8') in j[1] or '內容主題'.decode('utf8') in j[1]:
                if len(j[1].split(':')) < 3:
                    OIDLIST.append(i)
                    ErrorList.append((i, j[1]))

    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')

    return OIDLIST

def Check_error_4024(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []

    for i in Object:
        for j in Object[i]['Record']:
            if j[1] == 'N' or j[1] == 'A' or j[1] == 'n' or j[1] == '' or j[1] == ' ' or j[1] == '\n':
                OIDLIST.append(i)
                ErrorList.append((i, j[1]))
            elif '?' in j[1] or '？'.decode('utf8') in j[1] or '\n' in j[1]:
                OIDLIST.append(i)
                ErrorList.append((i, j[1]))

    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')
    return OIDLIST

'''
def Check_error_301(Object, colnames, FileName):
    
    OIDLIST = []
    DCTag = [line.strip() for line in open("DCTable.txt")]

    for i in Object:
        tag = list(set([t.split('::')[0] for t in Object[i].keys()]))
        for dctag in DCTag:
            if dctag not in tag:
                OIDLIST.append(i)
            dcwithfield = dctag + "::field"
            if dcwithfield not in tag:
                OIDLIST.append(i)

    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)

    return OIDLIST

def Check_error_302(Object, colnames, FileName):
    
    OIDLIST = []
    AdTag = [line.strip() for line in open("AdminDesc.txt")]
    DCTag = [line.strip() for line in open("DCTable.txt")]

    for i in Object:
        tag = list(set([t.split('::')[0] for t in Object[i].keys()]))
        CTag = AdTag + DCTag
        for t in tag:
            if t not in CTag:
                OIDLIST.append(i)

    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)

    return OIDLIST
'''


def Check_error_304(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []

    for i in Object:
        for j in colnames[1:]:
            if j in Object[i]:
                for k in Object[i][j]:
                    if k[1] == '' or k[1] == ' ':
                        OIDLIST.append(i)
                        ErrorList.append((i, "(%s:%s:%s)" %(i, j, k[1])))

    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')

    return OIDLIST

def Check_error_404(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []
    
    AdTag = [line.strip() for line in open("AdminDesc.txt")]
    DCTag = [line.strip() for line in open("DescMeta.txt")]

    for i in Object:
        tag = list(set([t.split('::')[0] for t in Object[i].keys()]))
        for t in tag:
            if t not in AdTag and t not in DCTag:
                OIDLIST.append(i)
                ErrorList.append((i, t))

        
    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')

    return OIDLIST

def Check_error_4041(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []

    for i in Object:
        for j in colnames[1:]:
            if j in Object[i]:
                for k in Object[i][j]:
                    if MatchFormat(k[1], '.*<.*>.*</.*>.*'):
                        OIDLIST.append(i)
                        ErrorList.append((i, k[1]))

    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')

    return OIDLIST

def Check_error_4042(Object, colnames, FileName):
    
    OIDLIST = []
    ErrorList = []
    element = ['field', 'dwc', 'license', 'Creator', 'GenDate']

    for i in Object:
        col = Object[i].keys()
        for j in col:
            if "::" in j:
                tag, field = j.split('::')
                if field not in element:
                    OIDLIST.append(i)
                    ErrorList.append((i, j))

    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0:
        MakeFile(Object, colnames, OIDLIST, FileName)
        MakeFile_withCon(Object, colnames, ErrorList, FileName[:-4] + '_withContent.csv')

    return OIDLIST

def ContentProblem(Object, colnames, item, FileName='', SpeciStr = []):

    OIDLIST = {}
    ErrorList = []

    if item in colnames:
        i = 0
        while colnames[i] != item:
            i += 1
    else:
        print 'No', item
        return []

    item = i+1

    for i in Object:
        if colnames[item-1] in Object[i]:
            for e, j in enumerate(Object[i][colnames[item-1]]):
                if j[1].startswith(' ') or j[1].endswith(' '):
                    #OIDLIST.append(i)
                    OIDLIST[i] = 1
                    ErrorList.append((i, j[1]))
                    #print j[1], 'space'
                
                if '?' in j[1] or '#' in j[1] or '{' in j[1] and not j[1].endswith('?'):
                    #OIDLIST.append(i)
                    OIDLIST[i] = 1
                    ErrorList.append((i, j[1]))
                    #print j[1], '?#'

                #if '\r\n' in j[1]:
                #    print j[1], 'newline'

                
                #guesslen, segmtoken = guess_seq_len(j[1])
                #if guesslen > 2:
                    #OIDLIST.append(i)
                    #ErrorList.append((i, j[1]))
                    #print i, e, j[1], '->', "::".join(segmtoken), guesslen
                
                for strs in SpeciStr:
                    if j[1] == strs:
                        OIDLIST.append(i)
                        ErrorList.append((i, j[1]))

    
    OIDLIST = list(set(OIDLIST))
    if len(OIDLIST) > 0 and FileName != '':
        MakeFile_withCon(Object, colnames, ErrorList, FileName)

    return OIDLIST

    
if __name__ == "__main__":
  
    #print "Start Loading data..."
    csvname = sys.argv[1]
    Object, colnames = Read("" + csvname + ".csv")
    
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d')
    #print "Now", st
    
    errorcode = {}
    errorDir = "error/" + csvname + "/err%d-" + st + ".csv"
    
    print "Start checking ..."
    errorcode['4011'] = Check_error_4011(Object, colnames, errorDir %(4011))
    errorcode['4012'] = Check_error_4012(Object, colnames, errorDir %(4012))
    errorcode['4021'] = Check_error_4021(Object, colnames, errorDir %(4021))
    errorcode['4022'] = Check_error_4022(Object, colnames, errorDir %(4022))
    errorcode['4023'] = Check_error_4023(Object, colnames, errorDir %(4023))
    errorcode['4024'] = Check_error_4024(Object, colnames, errorDir %(4024)) 
    
    errorcode['404'] = Check_error_404(Object, colnames, errorDir %(404))
    errorcode['4041'] = Check_error_4041(Object, colnames, errorDir %(4041))
    errorcode['4042'] = Check_error_4042(Object, colnames, errorDir %(4042))

    #errorcode['301'] = Check_error_301(Object, colnames, errorDir %(301))
    #errorcode['302'] = Check_error_302(Object, colnames, errorDir %(302))
    errorcode['304'] = Check_error_304(Object, colnames, errorDir %(304))
   
    for i in sorted(errorcode, key=int):
        if len(errorcode[i]) > 0:
            print "Error" + i + "\tFound"
        else:
            print "Error" + i + "\tNot Found"
    
    

    ContentDir = "error/" + csvname + "/%sProblem.csv"
    ConProb = {}
    ConProb['Title'] = ContentProblem(Object, colnames, 'Title', ContentDir %('Title'))
    ConProb['Description'] = ContentProblem(Object, colnames, 'Description', ContentDir %('Description'))
    ConProb['Record'] = ContentProblem(Object, colnames, 'Record', ContentDir %('Record'))
    ConProb['Contributor'] = ContentProblem(Object, colnames, 'Contributor', ContentDir %('Contributor'), ['\n', ' ', '-', '.', '#'])

    for i in sorted(ConProb):
        if len(ConProb[i]) > 0:
            print "Error %s found" %(i)
        else:
            print "Error %s not found" %(i)
